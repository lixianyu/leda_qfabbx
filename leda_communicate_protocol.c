#include "app_timer.h"
#include "mimas_bsp.h"
#include "config.h"
#include "leda_communicate_protocol.h"
#include "nrf_error.h"
#include <app_scheduler.h>
#include "app_uart.h"
#include "tethys_led.h"
#include "leda_file.h"
#include "leda_avr.h"
#include "leda_queue.h"

extern bool gUseRAMUpdateAVR;
extern ble_nus_t m_nus;
extern tethys_ble_nus_t m_tethys_nus;
extern uint8_t g_pstorage_buffer[32];
extern uint8_t gPartID;
uint32_t gBinSize = 0;
uint8_t gOTA = 0;
static uint32_t gBinRealSize;
static uint32_t gi = 0;
static uint32_t gAVSize = 0;

APP_TIMER_DEF(m_conn_timer_id);
bool gHaveUpdateCoonParamToMin = false;

#define BUILD_UINT32(Byte0, Byte1, Byte2, Byte3) \
          ((uint32_t)((uint32_t)((Byte0) & 0x00FF) \
          + ((uint32_t)((Byte1) & 0x00FF) << 8) \
          + ((uint32_t)((Byte2) & 0x00FF) << 16) \
          + ((uint32_t)((Byte3) & 0x00FF) << 24)))

#define MD_BLOCK_SIZE         16

static void conn_timeout_handler(void *p_context)
{
    leda_tell_app_transfer_bin(gAVSize);
}

void leda_init_communicate(void)
{
    app_timer_create(&m_conn_timer_id,
                      APP_TIMER_MODE_SINGLE_SHOT,
                      conn_timeout_handler);
}

bool is_all_number(char *pBuf, uint8_t len)
{
    for (int i = 0; i < len; i++)
    {
        if (*pBuf < '1' || *pBuf > '9')
        {
            return false;
        }
    }
    return true;
}

// return 1, can OTA.
uint8_t tethys_get_OTA(void)
{
    return gOTA;
}

static void resolve_OTA_Tethys(uint8_t *pBuf)
{
    switch (pBuf[2]) //key value
    {
        case 0x01://打开 OTA
        {
            gOTA = 1;
            g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
            g_pstorage_buffer[1] = 0x0D;
            g_pstorage_buffer[2] = 0x01;
            g_pstorage_buffer[3] = 0x01;
            g_pstorage_buffer[4] = 0x00;
            ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
        }
        break;
        
        case 0x02://关闭 OTA
        {
            gOTA = 0;
            g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
            g_pstorage_buffer[1] = 0x0D;
            g_pstorage_buffer[2] = 0x02;
            g_pstorage_buffer[3] = 0x01;
            g_pstorage_buffer[4] = 0x00;
            ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
        }
        break;
        
        default:
            break;
    }
}

#if 0
void leda_tell_app_transfer_bin(uint32_t avSize)
{
    gAVSize = avSize;
    if (gHaveUpdateCoonParamToMin)
    {
        leda_uart_uninit();
        elara_flash_write_init();
        g_pstorage_buffer[0] = (uint8_t)(avSize & 0xFF);
        g_pstorage_buffer[1] = (uint8_t)(avSize >> 8 & 0xFF);
        g_pstorage_buffer[2] = (uint8_t)(avSize >> 16 & 0xFF);
        g_pstorage_buffer[3] = (uint8_t)(avSize >> 24 & 0xFF);
        ble_nus_string_send(&m_nus, g_pstorage_buffer, 4);
    }
    else
    {
        app_timer_start(m_conn_timer_id, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
    }
}
#else
void leda_tell_app_transfer_bin(uint32_t avSize)
{
    leda_uart_uninit();
    elara_flash_write_init();
    g_pstorage_buffer[0] = (uint8_t)(avSize & 0xFF);
    g_pstorage_buffer[1] = (uint8_t)(avSize >> 8 & 0xFF);
    g_pstorage_buffer[2] = (uint8_t)(avSize >> 16 & 0xFF);
    g_pstorage_buffer[3] = (uint8_t)(avSize >> 24 & 0xFF);
    ble_nus_string_send(&m_nus, g_pstorage_buffer, 4);
}
#endif

/*****************************************************************************
* received content
*****************************************************************************/
void L1_receive_data_bin_size(ble_nus_t *p_nus, uint8_t *data, uint16_t length)
{
    //app_trace_dump(data, length);
    if (length == 5)
    {
        gUseRAMUpdateAVR = false;
        gBinSize = BUILD_UINT32(data[0], data[1], data[2], data[3]);
        if (gBinSize <= LEDA_QUEUE_SIZE)
        {
            gUseRAMUpdateAVR = true;
            gi = 0;
            gBinRealSize = ((gBinSize + LEDA_FLASH_WORD_SIZE - 1) / (uint32_t)LEDA_FLASH_WORD_SIZE) * (uint32_t)LEDA_FLASH_WORD_SIZE;//4
            leda_tell_app_transfer_bin(LEDA_QUEUE_SIZE);
            return;
        }
        uint32_t avSize = elara_flash_if_flash_enough(gBinSize);
        if (avSize != 0)
        {
            gi = 0;
            gBinRealSize = ((gBinSize + LEDA_FLASH_WORD_SIZE - 1) / (uint32_t)LEDA_FLASH_WORD_SIZE) * (uint32_t)LEDA_FLASH_WORD_SIZE;//4
            //gBinRealSize = gBinSize;
            if (avSize == 0xFFFFFFFF)
            {
                //mdProfile_AppCBs->pfnMDProfileChange( MDPROFILE_ERASE );
                elara_flash_erase();
                return;
            }
            else
            {
                close_LED(1);
            }
        }
        leda_tell_app_transfer_bin(avSize);
    }
    #if 0
    else if (data[0] == L1_HEADER_iBEACON_MAGIC)
    {
        //uint8_t len = data[3];
        if (data[1] == 0x88)
        {
            goto OHDoSwitch;
        }
        #if 0
        if (data[1] == 0x13)
        {
            goto OHDoSwitch;
        }
        if (data[1] == 0x12)
        {
            goto OHDoSwitch;
        }
        #endif
        
OHDoSwitch:
        switch(data[1])//Command ID
        {
            
            case 0x0D:
                resolve_OTA_Tethys(data);
                break;

            default:
                strcpy((char*)g_pstorage_buffer, "Who are you?");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 12);
                break;
        }
    }
    #endif
}

static void prepare_update_avr(void)
{
    //elara_flash_write_done();
    leda_update_avr_pre();

    char strTemp[32] = "Begin update";
    ble_nus_string_send_bin_blcok(&m_nus, (uint8_t*)strTemp, 12);
}

static void leda_tell_app_download_bin_finished(void)
{
    g_pstorage_buffer[0] = (uint8_t)(gi & 0xFF);
    g_pstorage_buffer[1] = (uint8_t)(gi >> 8 & 0xFF);
    g_pstorage_buffer[2] = (uint8_t)(gi >> 16 & 0xFF);
    g_pstorage_buffer[3] = (uint8_t)(gi >> 24 & 0xFF);

    uint32_t avSize = elara_flash_if_flash_enough(1);
    g_pstorage_buffer[4] = (uint8_t)(avSize & 0xFF);
    g_pstorage_buffer[5] = (uint8_t)(avSize >> 8 & 0xFF);
    g_pstorage_buffer[6] = (uint8_t)(avSize >> 16 & 0xFF);
    g_pstorage_buffer[7] = (uint8_t)(avSize >> 24 & 0xFF);
    ble_nus_string_send_bin_blcok(&m_nus, g_pstorage_buffer, 8);

    prepare_update_avr();
}

void L1_receive_data_bin_block(ble_nus_t *p_nus, uint8_t *data, uint16_t length)
{
    //invert_LED(3);
    //uint16_t block_size = MD_BLOCK_SIZE;
    uint16_t block_size;

    if (gi < gBinRealSize)
    {
        #if 1
        if (gBinRealSize - gi < MD_BLOCK_SIZE)
        {
            block_size = gBinRealSize - gi;
        }
        else
        {
            block_size = MD_BLOCK_SIZE;
        }
        #endif
        elara_flash_write(block_size, data);
    }

    gi += block_size;
    if (gi >= gBinRealSize)
    {
        leda_tell_app_download_bin_finished();
        //open_LED(1);
    }
}

void L1_receive_data_error_reset(ble_nus_t *p_nus, uint8_t *data, uint16_t length)
{
    #if 0
    gPartID = 0x86;
    ble_gatts_value_t gatts_value;

    // Initialize value struct.
    memset(&gatts_value, 0, sizeof(gatts_value));

    gatts_value.len     = sizeof(uint8_t);
    gatts_value.offset  = 0;
    gatts_value.p_value = &gPartID;
    sd_ble_gatts_value_set(p_nus->conn_handle, p_nus->part_id_handles.value_handle, &gatts_value);
    #endif
    uint8_t password[4] = {0x61,0x79,0xB3,0xAF};
    if (memcmp(data, password, 4) == 0)
    {
        // Reset nRF51822.
        NVIC_SystemReset();
    }
}

/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to 
 *          a string. The string will be be sent over BLE when the last character received was a 
 *          'new line' i.e '\n' (hex 0x0D) or if the string has reached a length of 
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */
static void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
    uint32_t       err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            //invert_LED(3);
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;

            if ((data_array[index - 1] == '\n') || (index >= (BLE_NUS_MAX_DATA_LEN)))
            {
                err_code = tethys_ble_nus_string_send(&m_tethys_nus, data_array, index);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
                
                index = 0;
            }
            break;

        case APP_UART_COMMUNICATION_ERROR:
            //APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            //APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}

#define LEDA_UART_RX_BUF_SIZE    32
#define LEDA_UART_TX_BUF_SIZE    32
void leda_uart_init(void)
{
    #if 0
    uint32_t err_code = NRF_SUCCESS;
    const app_uart_comm_params_t comm_params =
    {
        LEDA_RX_PIN,
        LEDA_TX_PIN,
        0xFF,
        0xFF,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud57600
    };

    APP_UART_FIFO_INIT( &comm_params,
                        LEDA_UART_RX_BUF_SIZE,
                        LEDA_UART_TX_BUF_SIZE,
                        uart_event_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);
    APP_ERROR_CHECK(err_code);
    #endif
}

void leda_uart_uninit(void)
{
#if 0    
    app_uart_close();

    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
}

//收到东西了，传给UART
void L1_receive_data_uart(tethys_ble_nus_t *p_nus, uint8_t *data, uint16_t length)
{
    //open_LEDs();
    for (uint8_t i = 0; i < length; i++)
    {
        while(app_uart_put(data[i]) != NRF_SUCCESS);
    }
}
