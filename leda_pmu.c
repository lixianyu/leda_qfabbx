/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "boards.h"
#include "nrf_drv_gpiote.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "tethys_led.h"
#include "leda_pmu.h"
#include "tethys_battery.h"

static void IO_VBUS_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
static void IO_CHRG_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
static void IO_VDD_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
#ifdef LEDA_LOWBAT_IO
static void IO_LOWBAT_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
#endif
static void pmu_timer_handler(void *p_context);
static void handle_pmu_timer(void);
static void handle_IO_VBUS_event(void);
static void handle_IO_CHRG_event(void);
static void handle_IO_VDD_event(void);
static void set_pin_no_work(uint32_t pin_number);
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
APP_TIMER_DEF(m_pmu_time_id);

#define POWER_LED 3 //Red
#define CHRG_LED 4 //Green
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
//#define PIN_IS_ON(pins_mask) ((pins_mask) & (NRF_GPIO->OUT ^ LEDS_INV_MASK) )
#if 0
__STATIC_INLINE uint32_t nrf_gpio_pin_state(uint32_t pin_number)
{
    uint32_t pin_state = ((NRF_GPIO->OUT >> pin_number) & 1UL);
    return pin_state;
}
#endif

void init_leda_pmu(void)
{
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;

    if (!nrf_drv_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        if (err_code != NRF_SUCCESS)
        {
            return;
        }
    }
    
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    err_code = nrf_drv_gpiote_in_init(IO_VBUS_PIN, &in_config, IO_VBUS_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_VBUS_PIN, true);

    in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    err_code = nrf_drv_gpiote_in_init(IO_VDD_PIN, &in_config, IO_VDD_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_VDD_PIN, true);

    app_timer_create(&m_pmu_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     pmu_timer_handler);
    app_timer_start(m_pmu_time_id, APP_TIMER_TICKS(7100, 0), NULL);
}

void leda_start_pmu_vbus_check(void)
{
    app_timer_start(m_pmu_time_id, APP_TIMER_TICKS(2100, 0), NULL);
}

static void init_IO_CHRG_pin(void)
{
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    uint32_t err_code = nrf_drv_gpiote_in_init(IO_CHRG_PIN, &in_config, IO_CHRG_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_CHRG_PIN, true);
}

static void uninit_IO_CHRG_pin(void)
{
    nrf_drv_gpiote_in_uninit(IO_CHRG_PIN);
}

static void init_IO_LOWBAT_pin(void)
{
#ifdef LEDA_LOWBAT_IO
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    uint32_t err_code = nrf_drv_gpiote_in_init(IO_LOWBAT_PIN, &in_config, IO_LOWBAT_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_LOWBAT_PIN, true);
#endif
}

static void uninit_IO_LOWBAT_pin(void)
{
#ifdef LEDA_LOWBAT_IO
    nrf_drv_gpiote_in_uninit(IO_LOWBAT_PIN);
#endif
}

static void init_IO_VBAT_pin(void)
{//ADC init.
    
}

static void uninit_IO_VBAT_pin(void)
{
    //set_pin_no_work(IO_VBAT_PIN);
}

static void pmu_timer_handler(void *p_context)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_pmu_timer);
}

static void handle_pmu_timer(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_VBUS_PIN))
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VBUS_event);
    }
    else
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VBUS_event);
    }
}

static void IO_VBUS_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VBUS_event);
}

static void IO_CHRG_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_CHRG_event);
}

static void IO_VDD_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VDD_event);
}

#ifdef LEDA_LOWBAT_IO
static void IO_LOWBAT_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_LOWBAT_event);
}
#endif

static void set_pin_no_work(uint32_t pin_number)
{
    nrf_gpio_cfg_default(pin_number);
}

static void handle_IO_VBUS_event(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_VBUS_PIN)) //USB插入
    {//此时无用的口是IO_VBAT_PIN, IO_LOWBAT_PIN
        init_IO_CHRG_pin();
        uninit_IO_LOWBAT_pin();
        uninit_IO_VBAT_pin();
        nrf_gpio_cfg(
            IO_PULLUP_PIN,
            NRF_GPIO_PIN_DIR_OUTPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
        nrf_gpio_pin_set(IO_PULLUP_PIN);
        
        
        handle_IO_VDD_event();
        handle_IO_CHRG_event();
    }
    else //USB拔出
    {//此时无用的口是IO_CHRG_PIN
        close_LED(CHRG_LED);
        uninit_IO_CHRG_pin();
        nrf_gpio_cfg(
            IO_PULLUP_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_CONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
        init_IO_VBAT_pin();
        init_IO_LOWBAT_pin();
        handle_IO_LOWBAT_event();
    }
}

static void handle_IO_CHRG_event(void)
{
    #if 1
    if (nrf_drv_gpiote_in_is_set(IO_CHRG_PIN))//充满
    {//充电LED常亮提示充满
        led_blink_stop();
        open_LED(CHRG_LED);
        //invert_LED(2);
    }
    else//充电中
    {//充电LED闪烁提示充电状态
        //invert_LED(1);
        led_blink(CHRG_LED);
    }
    #endif
}

static void handle_IO_VDD_event(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_VBUS_PIN))
    {
        if (nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED常亮
            open_LED(POWER_LED);
        }
    }
    else
    {
        handle_IO_LOWBAT_event();
    }
}

#ifdef LEDA_LOWBAT_IO
void handle_IO_LOWBAT_event(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_LOWBAT_PIN))//有电
    {
        led_blink_stop();
        if(nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED常亮
            open_LED(POWER_LED);
        }
    }
    else//低电量
    {
        //open_LED(1);
        //open_LED(2);
        if(nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            led_blink_stop();
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED闪烁提示低电量
            led_blink(POWER_LED);
            //led_pwm_start_1();
        }
    }
}
#else
void handle_IO_LOWBAT_event(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_VBUS_PIN)) //USB插入
    {
        return;
    }
    if (!tethys_if_low_battery())//有电
    {
        led_blink_stop();
        if(nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED常亮
            open_LED(POWER_LED);
        }
    }
    else//低电量
    {
        if(nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            led_blink_stop();
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED闪烁提示低电量
            led_blink(POWER_LED);
            //led_pwm_start_1();
        }
    }
}
#endif

