基于nRF51 SDK v10.0.0；
Base ble_app_proximity
Leda —— 木卫十三（木星的第十三个月亮）
S110 + bootloader + m328p升级用flash + Leda存储 =
96K  + 16K        + 32K              + 3K       = 147K

剩余Flash空间：(256K - 147K) / 2 = 109K / 2 = 54.5K = 55808 bytes
所以编译出来的bin不能超过55808字节，否则无法OTA了。
----------------------------------------------------------------------------------------------------
Program Size: Code=31292 RO-data=496 RW-data=460 ZI-data=9332
Program Size: Code=31304 RO-data=496 RW-data=460 ZI-data=9332
Program Size: Code=39496 RO-data=1204 RW-data=532 ZI-data=10300
Program Size: Code=39524 RO-data=1204 RW-data=544 ZI-data=10560
Program Size: Code=39524 RO-data=1204 RW-data=544 ZI-data=10776
Program Size: Code=39800 RO-data=1208 RW-data=556 ZI-data=10804
Program Size: Code=39976 RO-data=1208 RW-data=556 ZI-data=10804
Program Size: Code=40160 RO-data=1240 RW-data=560 ZI-data=10808
Program Size: Code=40572 RO-data=1248 RW-data=560 ZI-data=10824
Program Size: Code=41532 RO-data=1292 RW-data=568 ZI-data=11008
Program Size: Code=41604 RO-data=1292 RW-data=568 ZI-data=11008
Program Size: Code=41660 RO-data=1292 RW-data=568 ZI-data=11008
Program Size: Code=41796 RO-data=1304 RW-data=576 ZI-data=11008
Program Size: Code=42496 RO-data=1308 RW-data=580 ZI-data=11036
Program Size: Code=42868 RO-data=1296 RW-data=572 ZI-data=11036
Program Size: Code=40556 RO-data=1292 RW-data=572 ZI-data=11148
Program Size: Code=40556 RO-data=1292 RW-data=572 ZI-data=11340
Program Size: Code=40552 RO-data=1292 RW-data=572 ZI-data=11532
Program Size: Code=41524 RO-data=1276 RW-data=864 ZI-data=11192
Program Size: Code=41576 RO-data=1276 RW-data=580 ZI-data=11196
Program Size: Code=39724 RO-data=1276 RW-data=588 ZI-data=11196
Program Size: Code=39724 RO-data=1276 RW-data=588 ZI-data=11596
Program Size: Code=39724 RO-data=1276 RW-data=588 ZI-data=12996
Program Size: Code=39728 RO-data=1276 RW-data=588 ZI-data=16996
Program Size: Code=40172 RO-data=1292 RW-data=588 ZI-data=22576
Program Size: Code=42840 RO-data=1312 RW-data=580 ZI-data=22664
Program Size: Code=43504 RO-data=1332 RW-data=580 ZI-data=22684
Program Size: Code=43504 RO-data=1332 RW-data=580 ZI-data=22680
Program Size: Code=43496 RO-data=1332 RW-data=580 ZI-data=22680  
----------------------------------------------------------------------------------------------------
If there is a bootloader:
bootloader start address : 3C000, and size is 16KB
// swap
nrfjprog --memrd 0x0003BC00 --n 1024
// g_storage_handle_lxy
nrfjprog --memrd 0x0003B800 --n 1024

nrfjprog --memrd 0x00033C00 --n 1024
nrfjprog --memrd 0x00033800 --n 1024

// Device manager: m_storage_handle
nrfjprog --memrd 0x00033400 --n 1024

/////////////////////////////////////////////////////////////////////////////////////////////////////
If there is no bootloader:
nrfjprog --memrd 0x0003FC00 --n 1024 // swap
nrfjprog --memrd 0x0003F800 --n 1024 // g_storage_lock_handle
nrfjprog --memrd 0x0003F400 --n 1024 // g_storage_lock_handle0
nrfjprog --memrd 0x0003F000 --n 1024 // g_storage_handle_system_time
nrfjprog --memrd 0x0003EC00 --n 1024 // g_storage_handle_open_lock_history_pos
nrfjprog --memrd 0x0003E800 --n 1024 // g_storage_handle_open_lock_history
nrfjprog --memrd 0x0003E400 --n 1024 // g_storage_lock_handle5
nrfjprog --memrd 0x0003E000 --n 1024 // g_storage_lock_handle4
nrfjprog --memrd 0x0003DC00 --n 1024 // g_storage_lock_handle3
nrfjprog --memrd 0x0003D800 --n 1024 // g_storage_lock_handle2
nrfjprog --memrd 0x0003D400 --n 1024 // g_storage_lock_handle1
nrfjprog --memrd 0x0003D000 --n 1024 // g_storage_handle
nrfjprog --memrd 0x0003CC00 --n 1024 // Device manager: m_storage_handle

----------------------------------------------------------------------------------------------------
nrfutil dfu genpkg --application Leda_nrf51822_xxac_s110.hex --application-version 1 Leda_0.0.0306.01.zip

----------------------------------------------------------------------------------------------------
0xEE52C002A2F9
0xD0113D931A35